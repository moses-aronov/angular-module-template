import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; //Instead of Browser Module

import { ComponentName } from './components/name.component';
import { ServiceName } from './services/name.service';


@NgModule({
    imports: [
        CommonModule
    ],
    providers: [
        //Declare Services your module needs
        ServiceName
    ],
    declarations: [
        //Declare components that your module needs
        ComponentName
    ],
    exports: [
        //export the components that will be used by other applications
        //Try to seperate services, pipes, components, and directives into seperate modules
        ServiceName, ComponentName
    ]
    

})

export class ModuleName {

}