//Export your modules so other modules can access them

export { ModuleName } from './module'
export { ComponentName } from './components/name.component'
export { ServiceName } from './services/name.service'