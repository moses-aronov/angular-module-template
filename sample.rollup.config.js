export default {
    entry: 'dist/index.js',
    dest: 'dist/bundles/module.name.umd.js', //Set name here
    sourceMap: false,
    format: 'umd',
    moduleName: 'module.name', //Set name here
    globals: {
        //Set global imports here
        '@angular/core': 'ng.core',
        '@angular/common': 'ng.common'
    }
}