Angular Module

Sample Todo

- Create Any Services, Components that are needed
- Modify rollup to use proper name
- Review rollup.config.js before building
- Review packages.json before building
- Review dist/packages before publishing

Rollup configuration

Angular modules are delivered in UMD format, so your rollup.config.js should be set consequently.

The entry script is your transpiled index.ts, so it should match your TypeScript configuration. bundles/modulename.umd.js is the conventional path and name used by Angular modules.
Rollup requires a moduleName for the UMD format. It will be a JavaScript object, so do not use special characters (no dashes).

Your module use Angular things (at least the NgModule decorator), but your bundle should not include Angular.
Why? Angular will already be included by the user app. If your module includes it too, it will be there twice, and there will be fatal (and incomprehensible) errors.
So you need to set Angular as a global. And you need to know the UMD module name for each module. It follows this convention: ng.modulename (ng.core, ng.common, ng.http...).
Same goes for RxJS, if your module uses it. And module names are quite a mess here. For classes (Observable…), it’s Rx. For operators (map, filter…), it’s Rx.Observable.prototype. For direct methods of classes (of, fromEvent…), it’s Rx.Observable.